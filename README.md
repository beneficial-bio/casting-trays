# Gel Electrophoresis Casting Trays

General Casting Apparatus for most gel trays

Examples can be found [here](https://us.vwr.com/store/product/8026921/continuously-adjustable-horizontal-gel-casters-expedeon) and [here](https://www.transilluminators.com/products/multiple-gel-caster-for-rapidcast-and-flush-cut-gel-trays).

Need:
 - Baseplate
 - Levelling feet
 - Levelling bubble
 - Gasket
 - Fixed casting wall
 - Movable casting wall
 - Quick-Release Bar Clamps ([example](https://hackaday.com/2018/04/30/universal-quick-release-bar-clamps/))


## Educational Gel Tank

Examples:
https://2010.igem.org/Team:Baltimore_US/Notebook/EPInstructions

