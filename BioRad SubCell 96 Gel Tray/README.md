Replacement gel casting trays and combs needed for 3 x Bio-Rad Sub-Cell Model 96

### Gel Casting Tray

We adapted the [Parametric Gel Electrophoresis System](http://docubricks.com/viewer.jsp?id=3540694571097352192#brick_107883242) by Tobey Wenzel [see [DocuBrick Instructions](http://docubricks.com/viewer.jsp?id=3540694571097352192#brick_107883242)]

Here is the copy adjusted for [Bio-Rad Sub-Cell Model 96](https://cad.onshape.com/documents/11203dbe7629f9d8b888588c/w/513a34b7cbf6bbdec29b70c1/e/597e62de459a576a9187f151). Changes made:

 - All parts deleted except casting tray
 - Variable #gel_slot_width = 3 
 - Variable #gel_width = 250
 - Variable #gel_length = 150

 #### Files for 3D printing:

 - 2 x [STL for printing tray seal](https://gitlab.com/beneficial-bio/casting-trays/-/blob/master/BioRad%20SubCell%2096%20Gel%20Tray/Part_Studio_-_Tray_seal_sheet_mirrored.stl) 
 
  #### Files for laser cutting:

3mm Acrylic:

 - 1 x [Tray_bench_3mm.dxf](https://gitlab.com/beneficial-bio/casting-trays/-/blob/master/BioRad%20SubCell%2096%20Gel%20Tray/Laser%20Cutting%20Files/Tray_bench_3mm.dxf) 
 - 1 x [Tray_side_left_outer_3mm.dxf](https://gitlab.com/beneficial-bio/casting-trays/-/blob/master/BioRad%20SubCell%2096%20Gel%20Tray/Laser%20Cutting%20Files/Tray_side_left_outer_3mm.dxf)
 - 1 x [Tray_side_right_outer_3mm.dxf](https://gitlab.com/beneficial-bio/casting-trays/-/blob/master/BioRad%20SubCell%2096%20Gel%20Tray/Laser%20Cutting%20Files/Tray_side_right_outer_3mm.dxf)
 

5mm Acrylic:

 - 1 x [Tray_centre_5mm.dxf](https://gitlab.com/beneficial-bio/casting-trays/-/blob/master/BioRad%20SubCell%2096%20Gel%20Tray/Laser%20Cutting%20Files/Tray_centre_5mm.dxf)
 - 1 x [Tray_side_left_inner_5mm.dxf](https://gitlab.com/beneficial-bio/casting-trays/-/blob/master/BioRad%20SubCell%2096%20Gel%20Tray/Laser%20Cutting%20Files/Tray_side_left_inner_5mm.dxf)
 - 4 x [Tray_side_post_5mm.dxf](https://gitlab.com/beneficial-bio/casting-trays/-/blob/master/BioRad%20SubCell%2096%20Gel%20Tray/Laser%20Cutting%20Files/Tray_side_post_5mm.dxf)
 - 1 x [Tray_side_right_inner_5mm.dxf](https://gitlab.com/beneficial-bio/casting-trays/-/blob/master/BioRad%20SubCell%2096%20Gel%20Tray/Laser%20Cutting%20Files/Tray_side_post_5mm.dxf)
 
 #### Other parts:

  - Nitrile Rubber O Ring Cord 3.5mm diameter. We sourced from [eBay seller sealsuppliesuk](https://www.ebay.co.uk/itm/132126912032), price for 5m = £9.27

