// SLIDE HOLDER MODULE //
// This module is designed to be cut from acrylic

number_slides = 1;

slide_width = 52;
slide_length = 76;
slide_depth = 1.1;

frame_length = slide_length+6;
frame_height = 15;
acrylic_thickness = 1.5;
notch_length = slide_length+0.2;
notch_height = slide_depth+0.1;
notch_depth = frame_height/3;

// Inner layer of slide holder
   
difference() {
square([frame_length,frame_height], false);
    translate([(frame_length-notch_length)/2, ((frame_height/3)-notch_height)]) square([notch_length,notch_height], false);
    translate([5, (notch_depth*2)]) square([4.5,notch_depth], false);
}

// Outer layer of slide holder

translate([0,frame_height+5,0])
difference() {
square([frame_length,frame_height], false);
    translate([5, (notch_depth*2)]) square([4.5,notch_depth], false);
}

// COMB MODULE //

// Comb module variables
internal_width = slide_width-(3*acrylic_thickness);        // the width of just the comb, in mm (i.e. excluding the ears)
comb_height = frame_height/3;         // Height of each comb tooth in mm (or the depth of each well in the gel)
sample_lanes = 6;           // the number of lanes on the comb. Their width will adjust to fit evenly across the comb.
lane_spacing_width = 1.5;     // the gap in mm between each lane segment.
fixed_ladder_lanes = true;  // add lanes at each end for ladder (fixed width).
// Set this to true if you have very wide lanes and don't want to waste ladder in them.
// Set this to false and you'll have just the sample lanes across the comb.                            
body_height = 10;         // Height of the main 'body' part of the comb (the top section above the teeth)


module combs(number, spacing) {

       
ladder_width = 2; // in mm
width = ((internal_width - (2 * ladder_width + 2 * spacing)) - ((number - 1) * spacing)) / number ;
    

        
        // Left ladder lane
        translate([0, 0, 0]) square([ladder_width, comb_height + 1]);
        
        // dispense the sample lanes
        for (i = [1:number]) {
            translate([(0 - width + ladder_width) + (i * (width + spacing)), 0, 0]) square([width, comb_height + 1]);
        }
        
        // Right ladder lane
        translate([(internal_width - ladder_width),0,0]) square([ladder_width, comb_height + 1]);
        
                // Top of comb
        translate([-(((slide_width+4)-internal_width)/2), comb_height,0])
        square([slide_width+4,frame_height/3*2], false);
        
    }


translate([0,(2*(frame_height+5)),0])
combs(sample_lanes, lane_spacing_width);