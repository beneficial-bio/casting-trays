

 module tophalf(){
 linear_extrude(height=132.5, twist=0)
 polygon(points=[[0,0],[70,0],[70,-6.5],[45,-6.5],[0,-15]]);
 
 translate([5,-3,132.5])
 cylinder(h = 10, r1 = 2, r2 = 2, center = false);
 
 translate([60,-3,132.5])
 cylinder(h = 10, r1 = 2, r2 = 2, center = false);
};
 

 module bottomhalf(){
 translate([0,20,0])   
 difference(){
 
  linear_extrude(height=132.5, twist=0)
 polygon(points=[[0,0],[70,0],[70,-6.5],[45,-6.5],[0,-15]]);
 
 translate([5,-3,122.5])
 cylinder(h = 10, r1 = 2, r2 = 2, center = false);
 
 translate([60,-3,122.5])
 cylinder(h = 10, r1 = 2, r2 = 2, center = false);
 };
 };
 
 
 
 tophalf();
 bottomhalf();
 
 