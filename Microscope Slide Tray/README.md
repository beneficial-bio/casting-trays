Designed for use with 76 x 52 mm slides but there are [larger versions available](https://www.agarscientific.com/large-microscope-slides) and the design can be altered to suit.
