// Electrophoresis Comb Designer (http://www.thingiverse.com/thing:1275475) by TrenchCoatGuy is licensed under the Creative Commons - Attribution license. http://creativecommons.org/licenses/by/3.0/

/* [Gel Casting Tray Parameters] */
// Thickness of walls in mm
trayThickness = 5;
// Depth of comb slots
combSlotDepth = 5;
// Width of comb holders
combSlotWidth = 3;
// Gel Tray Height
gelTrayHeight = 17;

/* [Global] */
// Final height of the comb part
materialHeight = gelTrayHeight;
// Final width of the comb part
materialLength = 250;
// Final thickness of the comb part
materialThick = 2;

/* Comb Dimensions] */
// Center to center distance between combs
combRepeat = 9;
// Separation between combs
combGap = 1;
// Vertical length of the comb
combLength = gelTrayHeight/2;

/* [Side Spacer] */
// Offset for spacing the wells from the base
wellHeight = 1;
// Minimum extra space on the sides
sideSpacer = 3;




/* [Hidden] */
numCuts = floor((materialLength-2*sideSpacer)/combRepeat);
spacerWidth = (materialLength-numCuts*combRepeat)/2;

// Comb Holder
module makeCombHolder(){
    		difference()
		{
            translate([0,materialHeight+(2*trayThickness),0])
			cube([materialLength+(2*trayThickness),combSlotDepth+10,combSlotWidth],center=false);
    
            translate([materialLength/8+trayThickness,materialHeight+(2*trayThickness)+combSlotDepth,0])
            cylinder(5,materialThick,center=false);
            
            translate([materialLength/8*7+trayThickness,materialHeight+(2*trayThickness)+combSlotDepth,0])
            cylinder(5,materialThick,center=false);
        }
}

module makePart()
{
	difference()
	{
		cube([materialLength,materialHeight,materialThick],center=false);
		union()
		{
			// First Comb
			makeComb(numCuts,spacerWidth,combRepeat,combGap,combLength);
			// Additional spacer block (to shorten combs)
		translate([spacerWidth,-1,-materialThick/2])
		cube([materialLength-2*spacerWidth,wellHeight+1,materialThick*2],center=false);

//			// Fixing Holes
        translate([materialLength/8,materialHeight-combSlotDepth,0])
        cylinder(5,materialThick,center=false);
            
        translate([materialLength/8*7,materialHeight-combSlotDepth,0])
        cylinder(5,materialThick,center=false);

		}
	}
}

module makeComb(numCombs, xStart, xSpace, xGap, height)
{
	for (i=[1:numCombs+1])
	{
		translate([xStart+(i-1)*xSpace-xGap/2,wellHeight-1-wellHeight,-materialThick/2])
		cube([xGap,height+1+wellHeight,materialThick*2],center=false);
	}
}



//projection()
translate([0,0,-materialThickness/2])
projection(cut = true) makePart();
projection(cut = true) makeCombHolder();
